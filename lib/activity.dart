// My answr for the activity

import 'package:flutter/material.dart';

// Import statement needed, this deals with the widgets within flutter
void main() {
  runApp(App());

}

class App extends StatefulWidget {
  
  @override
  AppState createState() => AppState();
  // way to create a astate
}

class AppState extends State<App>{
  
  int questionIdx = 0;

  List<String> questions = [
    'What is the nature of your business needs?',
    'What is the expected size of the user base?',
    'In which region would the majority of the user be?',
    'What is the expected project duration?'
  ];

  List<List<String>> answers = [
    ['Time Tracking', 'Asset Management', 'Issue Tracking'],
    ['Less tan 1,000', 'Less than 10,000', 'More than 10,000'],
    ['Asia', 'Europe', 'Americas', 'Africa', 'Middle East'],
    ['Less than 3 months', '3-6 months', '6-9 months', '9-12 months', 'More than 12 months']
  ];

  void nextQuestion(){
    print(questionIdx);
    if(questionIdx < questions.length - 1){
      setState(() => questionIdx++);
    } 
  }
  
  @override
  Widget build(BuildContext context) {
    // The invisible widgetsa are the container and scaffold
    // The visible widgets are AppBar and Text

    /* 
    - To specify margin or padding spacing, we can use EdgeInsets
    - The values for spacing are multiples of 8(eg. 16,24,32)
    - To add spacing on all sides, use EdgeInsets.all()
    - to add spacing on certain sides, use EdgeInsets.only(direction:value)

    double.infinity - is used to tell the size that it will take up all the available size ex:
    width: double.infinity - sagad yung width na itetake nya sa screen
        almost same sya sa, double.maxFinite
        - double.infinity - refers to the infinity itself
        - double.maxFinite - refers to the maximum available fvalue of a double

    decoration: BoxDecoration(color: Colors.green) can be used to put colors on a container

    In a column widget: 
      - The MainAxisAlignment is a vertical (from top to bottom)
      - THe CrossAxisAlignment is used the horizontal postion or alignment of widget in a column 
      
    In the context of the column we can consider the column as vertical, and its cross axis as horizontal
     */
    Text questionText = Text(
      questions[questionIdx],
      style: TextStyle(
        fontSize: 24,
        fontWeight: FontWeight.bold
      )
    );

    List<RadioListTile> eachAnswer = answers[questionIdx].map((each){
    return RadioListTile<String>(
            title: Text(each),
            value: each,
            groupValue: null,
            onChanged: (value){
              nextQuestion();
              SnackBar snackBar = SnackBar(
                content: Text('You have selected $value'),
                duration: Duration(milliseconds: 2000),
                );
                ScaffoldMessenger.of(context).showSnackBar(snackBar);
               //ginawa to para pwede na din magawa yung function pag pinindot yung mga elements ng RadioList
            },
          );
  }).toList();


    Container body = Container(
      width: double.infinity, 
      padding: EdgeInsets.all(16),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          questionText,
           ...eachAnswer,
          Container(
            margin: EdgeInsets.only(top: 20),
            width: double.infinity,
            child: ElevatedButton(
                    child: Text('Skip Question'),
                    onPressed: nextQuestion, // the next question method is used here as a variable, it's a pointer for the method of nextQuestion
                    ),
          )
        ]
      ),
    );

    /* 
     The Scaffold widget provides basic design layout structure
      - it can be given UI elements such as an appbar
     */
    Scaffold homepage = Scaffold(
      appBar: AppBar(
        title: Text('Homepage'),),
      body: body
    );

    return MaterialApp(
        home: homepage
      );
  }
  /*  
  MaterialApp widget - came from the material.dart package
  parang gagawa din ng ainstance of  a MaterialApp
    return new MaterialApp() pero since dart, okay lang kahit wala ng new 

  Text widget - handles the rendering of the text input
  */
}

// Example of making a customize widget
class AnswerButton extends StatelessWidget {
  final String answers;
  final Function nextQuestion;
  // Constructor for AnswerButton
  AnswerButton({
    required this.answers,
    required this.nextQuestion
  });

  @override
  Widget build(BuildContext context) {
    return RadioListTile<String>(
            title: Text(answers),
            value: answers,
            groupValue: null,
            onChanged: (value){
              SnackBar snackBar = SnackBar(
                content: Text('You have selected $value'),
                duration: Duration(milliseconds: 2000),
                );
               //ginawa to para pwede na din magawa yung function pag pinindot yung mga elements ng RadioList
               ScaffoldMessenger.of(context).showSnackBar(snackBar);
            },
          );
  }
}

/* 
The build method in classes or widgets, is the responsible in generating or rendering the widgets
 */