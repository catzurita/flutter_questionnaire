import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:flutter_questionnaire/main.dart';

void main() {

    testWidgets('Questionaire app tests', (WidgetTester tester) async {

        // Loading the App to the test
        await tester.pumpWidget(App());

        // Find an option with text of "Asset Management" and click the option.
        expect(find.text('Asset Management'), findsOneWidget);
        await tester.tap(find.text('Asset Management'));
        await tester.pump();

        // Find an option with text of 'More than 10,000' and click the option.
        expect(find.text('More than 10,000'), findsOneWidget);
        await tester.tap(find.text('More than 10,000'));
        await tester.pump();

        // Find an option with text of "Europe" and click the option.
        expect(find.text('Europe'), findsOneWidget);
        await tester.tap(find.text('Europe'));
        await tester.pump();

        // Find an option with text of "6-9 months" and click the option.
        expect(find.text('6-9 months'), findsOneWidget);
        await tester.tap(find.text('6-9 months'));
        await tester.pump();

        // Summary of answers:
        expect(find.text('What is the nature of your business needs?'), findsOneWidget);
        expect(find.text('Asset Management'), findsOneWidget);

        expect(find.text('What is the expected size of the user base?'), findsOneWidget);
        expect(find.text('More than 10,000'), findsOneWidget);

        expect(find.text('In which region would the majority of the user be?'), findsOneWidget);
        expect(find.text('Europe'), findsOneWidget);

        expect(find.text('What is the expected project duration?'), findsOneWidget);
        expect(find.text('Europe'), findsOneWidget);

    });
}
